<%@ page import="fivehundred.Player" %>



<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="player.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${playerInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: playerInstance, field: 'nickName', 'error')} ">
	<label for="nickName">
		<g:message code="player.nickName.label" default="Nick Name" />
		
	</label>
	<g:textField name="nickName" value="${playerInstance?.nickName}"/>
</div>

